filesysobjects package
======================

Subpackages
-----------

.. toctree::

    filesysobjects.plugins

Submodules
----------

filesysobjects.apppaths module
------------------------------

.. automodule:: filesysobjects.apppaths
    :members:
    :undoc-members:
    :show-inheritance:

filesysobjects.configdata module
--------------------------------

.. automodule:: filesysobjects.configdata
    :members:
    :undoc-members:
    :show-inheritance:

filesysobjects.netfiles module
------------------------------

.. automodule:: filesysobjects.netfiles
    :members:
    :undoc-members:
    :show-inheritance:

filesysobjects.optionparser_fsysobj module
------------------------------------------

.. automodule:: filesysobjects.optionparser_fsysobj
    :members:
    :undoc-members:
    :show-inheritance:

filesysobjects.osdata module
----------------------------

.. automodule:: filesysobjects.osdata
    :members:
    :undoc-members:
    :show-inheritance:

filesysobjects.paths module
---------------------------

.. automodule:: filesysobjects.paths
    :members:
    :undoc-members:
    :show-inheritance:

filesysobjects.pathtools module
-------------------------------

.. automodule:: filesysobjects.pathtools
    :members:
    :undoc-members:
    :show-inheritance:

filesysobjects.pprint module
----------------------------

.. automodule:: filesysobjects.pprint
    :members:
    :undoc-members:
    :show-inheritance:

filesysobjects.userdata module
------------------------------

.. automodule:: filesysobjects.userdata
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: filesysobjects
    :members:
    :undoc-members:
    :show-inheritance:
